//
//  VMAppDelegate.h
//  EmojiKeyBoard
//
//  Created by Baljeet Singh on 28/08/14.
//  Copyright (c) 2014 Mine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
