//
//  main.m
//  EmojiKeyBoard
//
//  Created by Baljeet Singh on 28/08/14.
//  Copyright (c) 2014 Mine. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VMAppDelegate class]));
    }
}
