//
//  VMViewController.m
//  EmojiKeyBoard
//
//  Created by Baljeet Singh on 28/08/14.
//  Copyright (c) 2014 Mine. All rights reserved.
//

#import "VMViewController.h"
#import "AGEmojiKeyboardView.h"
@interface VMViewController ()<AGEmojiKeyboardViewDataSource,AGEmojiKeyboardViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *text;
@property (strong, nonatomic) IBOutlet UITextView *tv;

@end

@implementation VMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    AGEmojiKeyboardView *emojiKeyboardView = [[AGEmojiKeyboardView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 216) dataSource:self];
    emojiKeyboardView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    emojiKeyboardView.delegate = self;
    self.tv.inputView = emojiKeyboardView;
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)emojiKeyBoardView:(AGEmojiKeyboardView *)emojiKeyBoardView didUseEmoji:(NSString *)emoji {
    self.tv.text = [self.tv.text stringByAppendingString:emoji];
}

- (void)emojiKeyBoardViewDidPressBackSpace:(AGEmojiKeyboardView *)emojiKeyBoardView {
    
    [self.tv deleteBackward];
   
  }
//
//- (UIColor *)randomColor {
//    return [UIColor colorWithRed:drand48()
//                           green:drand48()
//                            blue:drand48()
//                           alpha:drand48()];
//}
//
//- (UIImage *)randomImage {
//    CGSize size = CGSizeMake(30, 10);
//    UIGraphicsBeginImageContextWithOptions(size , NO, 0);
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    UIColor *fillColor = [self randomColor];
//    CGContextSetFillColorWithColor(context, [fillColor CGColor]);
//    CGRect rect = CGRectMake(0, 0, size.width, size.height);
//    CGContextFillRect(context, rect);
//    
//    fillColor = [self randomColor];
//    CGContextSetFillColorWithColor(context, [fillColor CGColor]);
//    CGFloat xxx = 3;
//    rect = CGRectMake(xxx, xxx, size.width - 2 * xxx, size.height - 2 * xxx);
//    CGContextFillRect(context, rect);
//    
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return img;
//}

- (UIImage *)emojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView imageForSelectedCategory:(AGEmojiKeyboardViewCategoryImage)category {
   //AGEmojiKeyboardViewCategoryImageRecent
    //AGEmojiKeyboardViewCategoryImageFace
    //AGEmojiKeyboardViewCategoryImageBell
    //AGEmojiKeyboardViewCategoryImageFlower
    //AGEmojiKeyboardViewCategoryImageCar
    //AGEmojiKeyboardViewCategoryImageCharacters
    UIImage *img;
    switch (category) {
        case AGEmojiKeyboardViewCategoryImageRecent:
            img = [UIImage imageNamed:@"recent.png"];

            break;
        case AGEmojiKeyboardViewCategoryImageFace:
            img = [UIImage imageNamed:@"smiley.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageBell:
            img = [UIImage imageNamed:@"bell.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageFlower:
            img = [UIImage imageNamed:@"flower.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageCar:
            img = [UIImage imageNamed:@"transport.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageCharacters:
            img = [UIImage imageNamed:@"Numbers.png"];
            
            break;
        default:
            break;
    }
    
   // [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return img;
}

- (UIImage *)emojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView imageForNonSelectedCategory:(AGEmojiKeyboardViewCategoryImage)category {
    UIImage *img; 
    switch (category) {
        case AGEmojiKeyboardViewCategoryImageRecent:
            img = [UIImage imageNamed:@"recent.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageFace:
            img = [UIImage imageNamed:@"smiley.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageBell:
            img = [UIImage imageNamed:@"bell.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageFlower:
            img = [UIImage imageNamed:@"flower.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageCar:
            img = [UIImage imageNamed:@"transport.png"];
            
            break;
        case AGEmojiKeyboardViewCategoryImageCharacters:
            img = [UIImage imageNamed:@"Numbers.png"];
            
            break;
        default:
            break;
    }

    return img;
}

- (UIImage *)backSpaceButtonImageForEmojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView {
    UIImage *img  = [UIImage imageNamed:@"cancel.png"];;
    
    [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return img;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
